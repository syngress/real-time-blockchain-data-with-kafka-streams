# Real Time Blockchain Data With Kafka Streams

![RubyIMG](https://syngress.pl/images/blockchain_project_images/ruby_logo.png) ![Kafka](https://www.svgimages.com/svg-image/s9/apache-kafka-256x256.png) ![RubyIMG](https://syngress.pl/images/blockchain_project_images/docker_logo.png)

Things you may want to cover for local development:  

- Well it's still under construction  

# Preparation  

I'm sure you'll have to prepare something.

# Description of functionalities  

Project implements usage of Kafka Streams to send blockchain events.  
Blockchain is a technology that stores and transmits information about transactions concluded on the Internet.  
This information is arranged in the form of consecutive data blocks.  
One block contains information about certain number of transactions, then, after it is saturated and another block of data is created, followed by another and the next, creating a kind of chain. Information about various types of transactions, e.g. trading, buying or selling currencies, including cryptocurrencies, can be sent in that chain.  
Any computer connected to the Network can take part in sending and authenticating transactions.  
Thanks to complex cryptographic tools, data is fully secured against unauthorized access, and at the same time it is open to everyone.  
The user can view and verify the entire transaction history from the very beginning of the blockchain's existence up to the present day.  
Blockchain can be used to handle various transactions, e.g. trade, currencies. However, there is some attempts to use  blockchains for digital signature in state administration or as an accounting ledger in banking. These transactions can take place outside the centuries-old system, without the participation of public trust institutions, directly between the parties to the transaction.  

**COMMUNICATION MODEL**  
Most common communication pattern between systems is the synchronous, client-server model.  
System in this context are applications, microservices, databases and anything else that reads and writes data over a network.

![RTSsyncModelIMG](https://syngress.pl/images/blockchain_project_images/rts_sync_model.png)

In this case communication between components establishes an HTTP connection, and awaiting HTTP response.  
In case when two services communicating via REST, applications expect a specific HTTP response code with some data.  
Considering the above, applications are highly dependent on each other.  
However, when more systems need to communicate, point-to-point communication becomes difficult to scale and maintain.

![RTScomplexSyncModelIMG](https://syngress.pl/images/blockchain_project_images/rts_complex_sync_model.png)  

System become tightly coupled because their communication depends on knowledge of each other.  
Maintaining and updating systems will be more difficult than  it needs to be.  
Synchronous communication leaves little room for errors since there are no delivery guarantees if one of the systems goes offline.  
System may use different communications protocols, scaling strategies to deal with increased load, failure-handling strategies, etc.  
As a result, we may end up with multiple species of software applications to maintain, witch hurts maintainability.  
There isn't a strong notion for what is being communicated between these systems.  
Monoculture of the client-server model has put to much emphasis on request and responses, and not enough emphasis on the data itself.  
Communication is not replayable and it's difficult to reconstruct the state of a system.

**APACHE KAFKA**

Kafka solves above problems by acting as a centralized communication hub.  
Systems can send and receive data without knowledge of each other which is desirable in a real microservice architecture.  
In that case communication pattern is called `publish-subscribe` (bub/sub) from `pull-base` software architecture strategy.  

![KafkaArchitectureIMG](https://syngress.pl/images/blockchain_project_images/kafka_arch.png)

Kafka removes the complexity of point-to-potint communication.  
One system act as `producer` and other system act as `consumer`.  
Producers send data to Kafka topic, consumer connect to that topic and consume data for further processing.  
Same system can act as producer and consumer.  
Communication between systems is independent and asynchronous.  
Topics enable Kafka producers and Kafka consumers to be loosely coupled (isolated from each other), and are the mechanism that Apache Kafka uses to filter and deliver messages to specific consumers.  
Consumers subscribe to 1 or more topics of interest and receive messages that are sent to those topics by producers.  
Partitions are the main concurrency mechanism in Kafka.  
A topic is divided into 1 or more partitions, enabling producer and consumer loads to be scaled.  
Specifically, a consumer group supports as many consumers as partitions for a topic.  
The consumers are shared evenly across the partitions, allowing for the consumer load to be linearly scaled by increasing both consumers and partitions.  
You can have fewer consumers than partitions (in which case consumers get messages from multiple partitions), but if you have more consumers than partitions some of the consumers will be “starved” and not receive any messages until the number of consumers drops to (or below) the number of partitions. i.e. consumers don’t share partitions (unless they are in different consumer groups).  
There are several partitioning strategies, Kafka Clients provides three built-in strategies: Range, RoundRobin and StickyAssignor, you can read more about them here:  
https://medium.com/streamthoughts/understanding-kafka-partition-assignment-strategies-and-how-to-write-your-own-custom-assignor-ebeda1fc06f3  

**APACHE KAFKA ADVENTAGES**

So what are the benefits of implementation stream architecture ?  
System become decoupled and easier to maintain.  
Asynchronous communication comes with stronger delivery guarantees.  
System can standardize on the on the communication protocol (Kafka cluster use high-performance binary TCP protocol), as well as scaling strategies and fault-tolerance mechanisms.  
Consumer can process data at a rate they can handle. Unprocessed data is stored in Kafka, in a durable and fault-tolerant manner, till consumer is ready to process it.  
Systems can rebuild their state anytime by replaying message in a topic.

**Changing the way you think about communication between systems**

Streams are always flow one way, communication is not bidirectional like in client-server communication model.  
If system *A* produces some data to Kafka topic, and relies on another system *B* to do something with that data (ex. transform it), transformed data or response information will need to be written to another topic and subsequently consumed by the original process initiated by system *A*.  
This is simple to coordinate, but needs to change the way we think about communication.  

**ISOLATING COMPONENTS USING LINUX CONTAINER TECHNOLOGIES**  

Operation teams previously used virtual machines to run various components of the entire system.  
Several components running in the same virtual machine may require different and conflicting versions of the required libraries.  
Each of the components may have different environmental requirements.  
When the number of components grows, we cannot assign new virtual machine to each component if we do not want to waste resources and keep low hardware costs.  
Each virtual machine requires individual configuration and management, which increases the involvement of human resources in maintaining the entire environment.  
Instead of using virtual machines to isolate environments for each microservice, we can use linux container technology (LCT).  
LCT allow you to run multiple services on the same host, providing each of them a different environment, also isolating them from each other, with much less overhead.  
Process running in such a container runs in the native operating system like any other process (unlike virtual machines where the processes run on separate operating systems).  
From a process point of view, it looks as if it were one process running on same machine, in its own operating system.  

**VIRTUAL MACHINES VS LINUX CONTAINER TECHNOLOGIES**  

Linux containers are much lighter which allows to run more software components on the same hardware.  
Virtual machine has to run with its own set of system processes, which adds some extra overhead.  
Linux container is nothing more than a single, isolated process running on the native operating system that uses only the resources that the application consumes without the overhead of any additional processes.  
When there are three virtual machines in the host system, we have to deal with 3 completely separate systems, running on the same physical hardware.  
Below these virtual machines is the host operating system and hypervisor that distributes the physical hardware resources into smaller sets of virtual resources.  
Each set of virtual resources is used by the operating system inside each virtual machine.  
Applications running in virtual machines make system calls to the kernel of the guest operating system, which executes x86 instructions on the host physical processor via the hypervisor.  
Linux containers, on the other hand, perform system calls to the same kernel running on the host operating system, this single kernel is the only one that executes x86 instructions in the host processor. Using Linux Containers processor doesn't need any kind of virtualization like it does for virtual machines.  

**Virtual Machine**

![VirtualMachineIMG](https://syngress.pl/images/blockchain_project_images/rtb_virtual_machine.png)

**Linux Container**  

![LinuxContainerIMG](https://syngress.pl/images/blockchain_project_images/rtb_linux_container.png)

The main advantage of virtual machines is full isolation, each virtual machine runs its own kernel so it is a little bit secure than LCT where containers evoke the same kernel.  
With limited hardware resources, virtual machines perform well with few isolated processes.  
For running more isolated processes on the same machine, LCTs are a much better choice due to their lower overhead, process running in the container starts immediately.  

**Free Software, Hell Yeah!**  

**THIS PROJECT IS STILL UNDER CONSTRUCTION**
